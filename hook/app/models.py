from django.db import models

# Create your models here.

class Blog(models.Model):
    name = models.CharField(max_length=50, blank=True)
    text = models.TextField()

class Guests(models.Model):
    name = models.CharField(max_length=150, blank=True)

    def __str__(self):
        return str(self.id)+' | '+str(self.name)