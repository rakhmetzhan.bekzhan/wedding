from django.shortcuts import render, reverse
from django.http import HttpResponseRedirect
from django.views.generic import View
from .models import Blog, Guests
# Create your views here.

class IndexView(View):
    def get(self, request, g_id):
        g = Guests.objects.filter(id=g_id).first()
        objs = Blog.objects.all()
        return render(request, 'index.html', context={'objs':objs, 'g':g})
    def post(self, request, g_id):
        if "send" in request.POST:
            a = request.POST['name']
            b = request.POST['messages']
            obj = Blog(name=a, text=b)
            obj.save()
            return HttpResponseRedirect(reverse('index_page', kwargs={'g_id': g_id}))