from django.urls import path, include
from .views import *
from django.contrib.auth import views as auth_views


urlpatterns = [
    path('<int:g_id>', IndexView.as_view(), name="index_page"),
]